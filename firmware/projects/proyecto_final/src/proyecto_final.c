/** @mainpage Manejo de iluminacion de una habitacion mediante aplauso.
 *
 * \section genDesc General Description
 *
 * Esta aplicacion permite el encendido y apagado de una red de iluminacion de una habitacion mediante un aplauso,
 * informando el estado de las luces "Encendido" o "Apagado"
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	G	 	    | 	GNDA     	|
 * | 	AO 	        | 	CH1	        |
 * | 	+ 	        | 	VDDA	    |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 12/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Teresita Trucco
 *
 */

/** \brief Manejo de iluminacion de una habitacion mediante un aplauso con informe de estado de la misma por UART.
 **
 **
 **
 **/

/** \addtogroup ProyectoFinal
 ** @{ */
/** \addtogroup ProyectoFinal
 ** @{ */
/** \addtogroup ProyectoFinal
 ** @{ */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto_final.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "bool.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "iir.h"
#include "lowpass_clap.h"

/*==================[macros and definitions]=================================*/
#define PERIODO 3
#define UMBRALCLAP 200
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define abs(a) (((a) < (0)) ? (-a) : (a))
#define wL_HP 5
#define wL_LP 5

double w_HP[wL_HP];   	/*vector de estados internos del filtro pasa altos*/
double w_LP[wL_LP];   	/*vector de estados internos del filtro pasa bajos*/

/*==================[internal data definition]===============================*/

uint8_t texto[] = " \r\n";
uint8_t texto_encendido[] = "Iluminacion: Encendida ";
uint8_t texto_apagado[] = "Iluminacion: Apagada ";

uint16_t dato;
double senial;
double senial_postlow;
bool contador=false;
bool convert;
bool onoff;

/*==================[internal functions declaration]=========================*/
/** @fn void TemporizadorIn(void);
 * @brief Inicializa la lectura analogica y la conversion AD. Filtra con un filtro pasa bajos la señal de entrada.
 * @return  */
void TemporizadorIn(void);
/** @fn void MensajeInformativo(void);
 * @brief Informa con un mensaje por puerto serie si la iluminacion esta encendida o apagada.
 * @return  */
void MensajeInformativo(void);
/** @fn void EncenderLeds(void);
 * @brief Enciende o apaga los leds de la EDU-CIAA segun el estado en que se supero el umbral.
 * @return  */
void EncenderLeds(void);

/*==================[external data definition]===============================*/

//Estructura para inicializar conversion
analog_input_config ad_config = {CH1, AINPUTS_SINGLE_READ, NULL};


//Estructura para inicializar timer
timer_config temporizador =  {TIMER_A, PERIODO, &TemporizadorIn};


//Estructura para inicializar uart
serial_config puerto_serie =  {SERIAL_PORT_PC, 115200, NULL};

/*==================[external functions definition]==========================*/



void InterrupcionTecla1() {
	onoff = !onoff;
	LedOff(LED_RGB_G);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);

}

void TemporizadorIn(void)
{
	AnalogInputRead(CH1, &dato);
	senial = ((double) dato/1023.0)*3.3;
	convert=true;
	/*Filtro pasa bajos de la señal mediante filtro IIR*/
	senial_postlow = iir(DL_LP - 1, DEN_LP, NL_LP - 1, NUM_LP, w_LP, senial);
	dato= (uint16_t)((senial_postlow*1023.0)/3.3);
	AnalogStartConvertion();

}

void EncenderLeds(void){
	if(convert == true){

		if (onoff == true){
			LedOn(LED_RGB_G);


			convert=false;
			if (UMBRALCLAP>200){
				LedOn(LED_RGB_G);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);

			}
			contador=true;
			MensajeInformativo();


			if (UMBRALCLAP>200 && contador==true){

				contador=false;
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				MensajeInformativo();


			}
		}


	}
}

void MensajeInformativo(void){
	if(contador==true){
		UartSendString(puerto_serie.port, texto_encendido);
		UartSendString(puerto_serie.port, texto);

	}
	if(contador==false){
		UartSendString(puerto_serie.port, texto_apagado);
		UartSendString(puerto_serie.port, texto);
	}
}


int main(void){


	SystemClockInit();
	LedsInit();
	AnalogInputInit(&ad_config);
	AnalogStartConvertion();
	TimerInit(&temporizador);
	TimerStart(TIMER_A);
	UartInit(&puerto_serie);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, InterrupcionTecla1);





	while(1){

		EncenderLeds();

	}






	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
