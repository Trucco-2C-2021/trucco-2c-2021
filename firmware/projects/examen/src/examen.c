/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL2	|
 * | 	Trigger	 	| 	GPIO_T_FIL3	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/examen.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUDIOS 115200
#define MAXANG 2.1
#define MINANG 1.2
#define PERIODO 70
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void SistInit(void);
void ConversionAD(void);
void CalculoDeDistancia(void);
void LecturaVolt(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A, PERIODO, &ConversionAD};
analog_input_config entrada = {CH1, AINPUTS_SINGLE_READ, &LecturaVolt};
float valor=0;
uint8_t voltaje=0;
uint8_t contador_volt=0;
uint8_t contador_ang=0;
uint8_t ang_actual=0;
uint8_t ang_anterior=0;
uint8_t distancia_cm=0;
uint8_t minima_distancia;
uint8_t aux_min;


/*==================[external functions definition]==========================*/

void ConversionAD(void) {

	AnalogStartConvertion();

}
//Se calcular la distancia en cm al objeto.
void CalculoDeDistancia(void)
{
	distancia_cm = HcSr04ReadDistanceInCentimeters();

}

//Se envia por puerto serie la distancia a un objeto para cada angulo.
void LecturaVolt(void) {
	AnalogInputRead(CH1, &voltaje);

	if (voltaje< MAXANG && voltaje > MINANG) {
		if (voltaje == MINANG){

			   contador_ang=0;
			   contador_volt=MINANG;
			   CalculoDeDistancia();
			   UartSendString(SERIAL_PORT_PC, UartItoa(contador_ang, 10));
				UartSendString(SERIAL_PORT_PC, "°C objeto a ");
				UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm, 10));
				UartSendString(SERIAL_PORT_PC, "cm \r\n");
			   contador_volt=contador_volt+0.15;
			   contador_ang=contador_ang+15;
			   CalculoDeDistancia();
			   UartSendString(SERIAL_PORT_PC, UartItoa(contador_ang, 10));
				UartSendString(SERIAL_PORT_PC, "°C objeto a ");
				UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm, 10));
				UartSendString(SERIAL_PORT_PC, "cm \r\n");
			   contador_volt=contador_volt+0.15;
			   contador_ang=contador_ang+15;
			   CalculoDeDistancia();
			   UartSendString(SERIAL_PORT_PC, UartItoa(contador_ang, 10));
			   UartSendString(SERIAL_PORT_PC, "°C objeto a ");
			   UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm, 10));
			   UartSendString(SERIAL_PORT_PC, "cm \r\n");
			   contador_volt=contador_volt+0.15;
			   contador_ang=contador_ang+15;
			   CalculoDeDistancia();
			   UartSendString(SERIAL_PORT_PC, UartItoa(contador_ang, 10));
			   UartSendString(SERIAL_PORT_PC, "°C objeto a ");
			   UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm, 10));
			   UartSendString(SERIAL_PORT_PC, "cm \r\n");
			   contador_volt=contador_volt+0.15;
			   contador_ang=contador_ang+15;
			   CalculoDeDistancia();
			   UartSendString(SERIAL_PORT_PC, UartItoa(contador_ang, 10));
			   UartSendString(SERIAL_PORT_PC, "°C objeto a ");
			   UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm, 10));
			   UartSendString(SERIAL_PORT_PC, "cm \r\n");
			   contador_volt=contador_volt+0.15;
			   contador_ang=contador_ang+15;
			   CalculoDeDistancia();
			   UartSendString(SERIAL_PORT_PC, UartItoa(contador_ang, 10));
			   UartSendString(SERIAL_PORT_PC, "°C objeto a ");
			   UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm, 10));
			   UartSendString(SERIAL_PORT_PC, "cm \r\n");

			   //Cálculo de mínimo

			    distancias[valores_distancias] = distancia_cm;
			   			   valores_distancias++;
			   				if(aux_min > distancia_cm){
			   					aux_min = distancia_cm;
			   				}

			   				else {
			   				   minima_distancia= aux_min;
				   			   UartSendString(SERIAL_PORT_PC, "Obstaculo en ");
				   			   UartSendString(SERIAL_PORT_PC, UartItoa(minima_distancia, 10));
				   			   UartSendString(SERIAL_PORT_PC, "cm ");
			   				   minima_distancia=0;
			   				}

		}



	}


}


void SistInit(void) {
	SystemClockInit();
	LedsInit();
	/*T_FIL2 ->pin echo, T_FIL3 -> pin trigger */
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	TimerInit(&my_timer);
	AnalogInputInit(&entrada);
	serial_config medicion = { SERIAL_PORT_PC, BAUDIOS, NULL };
	UartInit(&medicion);

	TimerStart(TIMER_A);
}

int main(void){
	SistInit();

    while(1){
    	//encendiendo el led rojo del RGB para indicar que se está girando a la derecha y el led verde del RGB para indicar el giro hacia la izquierda
    	if (ang_anterior < ang_actual) {
    				LedOn(LED_RGB_B);
    				LedOff(LED_1);
    				LedOff(LED_2);
    				LedOff(LED_3);

    			}
    			if (ang_actual < ang_anterior) {
    				LedOn(LED_RGB_G);
    				LedOff(LED_1);
    				LedOff(LED_2);
    				LedOff(LED_3);
    			}

	}
    
	return 0;
}

/*==================[end of file]============================================*/

