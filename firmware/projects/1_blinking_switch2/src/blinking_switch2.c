/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 2 - Ejercicio 2.2
 *
 * Objetivo: El led dos se encontrara encendido. Al presionar la telca 2 titila mas lento
 * y al presionar la tecla 4 lo hara mas lento.
 *
 *
 *
 *


/*==================[inclusions]=============================================*/
#include "../inc/blinking_switch2.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
#define COUNT_DELAY2 10

/*==================[internal data definition]===============================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		asm  ("nop");
	}
}

void Delay2(void)
{
	uint32_t i;

	for(i=COUNT_DELAY2; i!=0; i--)
	{
		asm  ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();


	while(1)
	{
		LedOn(LED_2);
		Delay();
		LedOff(LED_2);
		Delay();
		teclas = SwitchesRead();
		switch (teclas) {
		case SWITCH_2:
			LedOn(LED_2);
			Delay();
			Delay();

			LedOff(LED_2);
			Delay();
			Delay();

			break;
		case SWITCH_4:
			LedOn(LED_2);
			Delay2();
			LedOff(LED_2);
			Delay2();
			break;


		}
	}

}

/*==================[end of file]============================================*/

