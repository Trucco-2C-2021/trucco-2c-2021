/** @mainpage Medidor de distancia con envio de datos por UART
 *
 * \section genDesc General Description
 *
 * Esta aplicacion mide distancia con el sensor hc_sr4 y envia las mediciones para mostrar por UART-USB.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL2	|
 * | 	Trigger	 	| 	GPIO_T_FIL3	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 24/9/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Teresita Trucco
 *
 */

/** \brief Medidor de distancia con interrupciones y visualizacion de datos UART-USB.
 **
 **
 **
 **/

/** \addtogroup Proyecto3UART
 ** @{ */
/** \addtogroup Proyecto3UART
 ** @{ */
/** \addtogroup Proyecto3UART
 ** @{ */


/*==================[inclusions]=============================================*/

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "medidor_dist_uart.h"

/*==================[macros and definitions]=================================*/
#define BAUDIOS 115200
bool onoff = false; /**< Controla el encendido/apagado de la medición.*/
bool hold = false; /**<Controla cuando la medición se detiene.*/
bool timer_control = false; /**<Variable de estado para controlar el funcionamiento del timer*/
uint16_t distancia;
uint16_t distancia_hold;
uint8_t tecla_recibida;
/*==================[internal data definition]===============================*/
void do_timer(void);
void do_uart(void);

/** @fn  void InterrupcionTecla1();
 * @brief Interrupción para cuando se presione switch 1. Modifica la variable de estado onoff.
 * @param[in] Sin parámetros.
 * @return .
 */
void InterrupcionTecla1() {
	onoff = !onoff;
}

/** @fn void InterrupcionTecla2();
 * @brief	Interrupción para cuando se presione switch 2.  Modifica la variable de estado hold.
 * @param[in] Sin parámetros.
 * @return .
 */
void InterrupcionTecla2() {
	hold = !hold;
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
timer_config my_timer = { TIMER_A, 1000, &do_timer };
serial_config my_serial_port = {SERIAL_PORT_PC, BAUDIOS, &do_uart};

/*==================[external functions definition]==========================*/
void do_timer() {
	timer_control = true;
}

/** @fn  do_uart();
 * @brief Permite en encendido/apagado y la pausa de una medicion mediante las teclas "h" y "o" del teclado.
 * @param[in] Sin parámetros.
 * @return .
 */
void do_uart() {

	UartReadByte(my_serial_port.port, &tecla_recibida);
	if (tecla_recibida == 'o') {
		onoff = !onoff;
	}

	if (tecla_recibida == 'h') {
		hold = !hold;
	}

}

int main(void) {
	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	/*T_FIL2 ->pin echo, T_FIL3 -> pin trigger */
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);

	/* inicialización del timer*/
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	SwitchActivInt(SWITCH_1, InterrupcionTecla1);
	SwitchActivInt(SWITCH_2, InterrupcionTecla2);
	UartInit(&my_serial_port);
	uint8_t texto[] = " cm \r\n";
	while (1) {
		if (timer_control == true) {
			if (onoff == true) {
				distancia = HcSr04ReadDistanceInCentimeters();
				if (hold == false) {
					distancia_hold = distancia;
					if (distancia_hold > 0 && distancia_hold <= 10) { //Si la distancia está entre 0 y 10 cm, onoff el LED_RGB_B (Azul).
						LedOn(LED_RGB_B);
						LedOff(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 10 && distancia_hold <= 20) {
						//Si la distancia está entre 10 y 20 cm, onoff el LED_RGB_B (Azul) y LED_1.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 20 && distancia_hold <= 30) {
						// Si la distancia está entre 20 y 30 cm, onoff el LED_RGB_B (Azul), LED_1 y LED_2
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 30) {
						//Si la distancia es mayor a 30 cm, onoff el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_3);
					}

				} //end if hold=false

				/**
				 * @brief UartSendString();
				 * Lineas a visualizar en hterm. Envio de datos nuevamente a la pc.*/
				UartSendString(my_serial_port.port,UartItoa(distancia_hold,10));
				UartSendString(my_serial_port.port, texto);
			} //end if de onoff=true

			else {
				LedOff(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			timer_control = false;

		} // end del if del timer_control

	} //end del while

	return 0;
} //end del main

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
