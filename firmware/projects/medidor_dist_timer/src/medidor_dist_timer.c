/** @mainpage Medidor de distancia mediante interrupciones
 *
 * \section genDesc General Description
 *
 * Mide la distancia con el sensor hc_sr4 y manipula las mediciones mediante interrupciones.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL2	|
 * | 	Trigger	 	| 	GPIO_T_FIL3	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 24/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Teresita Trucco
 *
 */

/** \brief Medidor de distancia con interrupciones.
 **
 **
 **
 **/

/** \addtogroup Proyecto3
 ** @{ */
/** \addtogroup Proyecto3
 ** @{ */
/** \addtogroup Proyecto3
 ** @{ */

/*==================[inclusions]=============================================*/

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "medidor_dist_timer.h"

/*==================[macros and definitions]=================================*/
bool onoff = false; /**< Controla el encendido/apagado de la medición.*/
bool hold = false; /**<Controla cuando la medición se detiene.*/
bool timer_control = false; /**<Variable de estado para controlar el funcionamiento del timer*/
uint16_t distancia;
uint16_t distancia_hold;
/*==================[internal data definition]===============================*/
void do_timer(void);

/** @fn  void InterrupcionTecla1();
 * @brief Interrupción para cuando se presione switch 1. Modifica la variable de estado onoff.
 * @param[in] Sin parámetros.
 * @return .
 */
void InterrupcionTecla1() {
		onoff = !onoff;

}
/** @fn void InterrupcionTecla2();
 * @brief	Interrupción para cuando se presione switch 2.  Modifica la variable de estado hold.
 * @param[in] Sin parámetros.
 * @return .
 */
void InterrupcionTecla2() {
		hold = !hold;

}

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A, 1000, &do_timer};   //1000 valor en ms (1s)

/*==================[external functions definition]==========================*/
void do_timer()
{timer_control = true;}

int main(void)
{

	/** @brief SystemClockInit();
	        LedsInit();
	        SwitchesInit();
	        HcSr04Init();
	 * Inicialización de los periféricos y dispostivos.
	 * @param[in] Sin parámetros.
     * @return .
	*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);

	/**
	 *  @brief SwitchActivInt()
	 *  Inicialización de las interrupciones
     * @return .
 */
	SwitchActivInt(SWITCH_1, InterrupcionTecla1);
	SwitchActivInt(SWITCH_2, InterrupcionTecla2);

	/**
	 * @brief
	 * TimerInit(&my_timer);
	 * TimerStart(TIMER_A);
	 * Inicialización del timer*/
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	while (1) {
		if (timer_control == true) {
			if (onoff == true) {
				distancia = HcSr04ReadDistanceInCentimeters();
				if (hold == false) {
					distancia_hold = distancia;
					if (distancia_hold > 0 && distancia_hold <= 10) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
						LedOn(LED_RGB_B);
						LedOff(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 10 && distancia_hold <= 20) {
						//Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y LED_1.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 20 && distancia_hold <= 30) {
						// Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED_1 y LED_2
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 30) {
						//Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_3);
					}

				} //end if hold=false

			} //end if de onoff=true

			else {
				LedOff(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			timer_control = false;
		}

	} //while


	return 0;
} //main

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


