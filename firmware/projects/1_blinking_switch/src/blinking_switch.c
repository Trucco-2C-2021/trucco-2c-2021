/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 2 - Ejercicio 2
 *
 * Objetivo: Modifique la aplicación 1_blinking_switch de manera de hacer titilar los leds verde,
 * amarillo y rojo al mantener presionada las teclas 2, 3 y 4 correspondientemente.
 * También se debe poder hacer titilar los leds individuales del led RGB, para ello se
 * deberá mantener presionada la tecla 1 junto con la tecla 2, 3 o 4 correspondientemente.
 *
 *

/*==================[inclusions]=============================================*/
#include "blinking_switch.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		asm  ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	//LedOn(LED_2);
	while(1)
	{

		//Agrego
	/*  LedOn(LED_3);
		Delay();
		LedOn(LED_2);
		Delay();
		LedOn(LED_1);
		Delay();
		LedOn(LED_RGB_B);
		Delay();
		LedOff(LED_3);
		LedOff(LED_2);
		LedOff(LED_1);
		LedOff(LED_RGB_B);
		Delay();
		*/

		teclas  = SwitchesRead();
		switch(teclas)
		{
		case SWITCH_2:
			LedOn(LED_3);
			Delay();
			LedOff(LED_3);
			Delay();
			break;
		case SWITCH_3:
			LedOn(LED_2);
			Delay();
			LedOff(LED_2);
			Delay();
			break;
		case SWITCH_4:
			LedOn(LED_1);
			Delay();
			LedOff(LED_1);
			Delay();
			break;
		case (SWITCH_1 | SWITCH_2):
			LedOn(LED_RGB_R);
			Delay();
			LedOff(LED_RGB_R);
			Delay();
			break;
		case (SWITCH_1 | SWITCH_3):
			LedOn(LED_RGB_G);
			Delay();
			LedOff(LED_RGB_G);
			Delay();
			break;
		case (SWITCH_1 | SWITCH_4):
			LedOn(LED_RGB_B);
			Delay();
			LedOff(LED_RGB_B);
			Delay();
			break;

		}
	}

}

/*==================[end of file]============================================*/

