/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/medidor_dist.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "hc_sr4.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void) {

	bool onoff = false; // Controla el encendido/apagado de la medición.
	bool hold = false; // Controla cuando la medición se detiene.

	uint8_t teclas, tecla_anterior;
	uint16_t distancia = 0;
	uint16_t distancia_hold = 0;

	/*Inicialización de periféricos y dispostivos*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	while (1) {

		teclas = SwitchesRead();
				if (teclas != tecla_anterior) {
					switch (teclas) {
					case SWITCH_1: {
						onoff = !onoff;
						break;
					}
					case SWITCH_2: {
						hold = !hold;
						break;
					}
					} //switch
				} // if


		tecla_anterior=teclas;
		if (onoff == true) {
			distancia = HcSr04ReadDistanceInCentimeters();

		if (hold == false) {
			distancia_hold = distancia;

			//distancia = HcSr04ReadDistanceInCentimeters();
			//distancia_hold = distancia;

			if (distancia_hold > 0 && distancia_hold <= 10) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
				LedOn(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			if (distancia_hold > 10 && distancia_hold <= 20) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul) y LED1.
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			if (distancia_hold > 20 && distancia_hold <= 30) { //Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED1 y LED2.
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOff(LED_3);
			}
			if (distancia_hold > 30 && distancia_hold <= 130) { //Si la distancia es mayor a 30cm encende todos los leds
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			}


		} //if hold
		} //if onfoff
		else {
					LedOff(LED_RGB_B);
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
		}


DelayMs(200);

	} //while

	return 0;
}

/*==================[end of file]============================================*/

