/** @mainpage Driver filtro IIR
 *
 * \section genDesc General Description
 *
 * Filtro de tipo IIR
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 12/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Teresita Trucco
 *
 */

/** \brief Driver estructura filtro IIR
 **
 **
 **
 **/

/** \addtogroup DriverMicroncontroller
 ** @{ */
/** \addtogroup DriverMicroncontroller
 ** @{ */
/** \addtogroup DriverMicroncontroller
 ** @{ */

#ifndef CAN_H_
#define CAN_H_

double iir(int M,const double *a,int L,const double *b,double *w,double x);


#endif /* CAN_H_ */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */


/*#endif #ifndef IIR_H */
