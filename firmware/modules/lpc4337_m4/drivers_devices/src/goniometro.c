/* Copyright 2019,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal driver for switchs in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  SM			Sebastian Mateos
 *  EF			Eduardo Filomena
 *  JMR			Juan Manuel Reta
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version leo
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "delay.h"
#include "bool.h"
#include "gpio.h"
#include "../../drivers_devices/inc/goniometro.h" //own header
/*==================[macros and definitions]=================================*/

#define PULSO_TRIGGER 10
#define PULSO_ECHO 1

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

//Variables de estado de los pines
bool HIGH = true;
bool LOW = false;

//Echo y trigger inicializados
gpio_t pin_echo;
gpio_t pin_trigger;

uint8_t angulo=0;
uint8_t cont_angulo;
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
bool GoniometroInit(gpio_t echo, gpio_t trigger)
{
	pin_echo = echo;
	pin_trigger = trigger;
// Se configuran los puertos como entrada y salida
	GPIOInit(pin_echo, GPIO_INPUT);
	GPIOInit(pin_trigger, GPIO_OUTPUT);

	return true;
}



uint16_t GoniometroAngEnGrados(void){
			GPIOOn(pin_trigger);// pongo en alto el trigger
			DelayUs(PULSO_TRIGGER); // delay de 10 us
			GPIOOff(pin_trigger);// pongo en cero el trigger

	while (GPIORead(pin_echo) == LOW){}
	while (GPIORead(pin_echo) == HIGH)
	{
		DelayUs(PULSO_ECHO);
		cont_angulo++;
	}

	angulo = cont_angulo / 75;
    cont_angulo=0;
	return angulo;

}

bool GoniometroDeinit()
{
	return true;
}
