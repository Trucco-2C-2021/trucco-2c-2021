/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio 14
 * 
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"

/*==================[macros and definitions]=================================*/

typedef struct {
	uint8_t apellido[20];
	uint8_t nombre[12];
	uint8_t edad;

} alumno;
alumno alumno_1;
alumno alumno_2;
alumno *alum;
uint8_t ape[] = "Perez";
uint8_t nom[] = "Raton";
uint8_t ape2[] = "Noel";
uint8_t nom2[] = "Papa";

/*==================[internal functions declaration]=========================*/

int main(void) {

    //Inciso A
	alumno_1.edad = 25;
	uint8_t i;
	printf("Edad alumno 1: %d\r\n", alumno_1.edad);

	for (i = 0; i < 20; i++) {

		alumno_1.apellido[i] = ape[i];
		alumno_1.nombre[i] = nom[i];

	}

	printf("Apellido alumno 1: %s \r\n", alumno_1.apellido);
	printf("Nombre alumno 1: %s \r\n", alumno_1.nombre);

	//Inciso B
	alum = &alumno_2;
	alum->edad = 23;
	uint8_t j;
	printf("Edad alumno 1: %d\r\n", alumno_2.edad);

	for (j = 0; j < 20; j++) {

		alumno_2.apellido[j] = ape2[j];
		alumno_2.nombre[j] = nom2[j];

	}

	printf("Apellido alumno 2: %s \r\n", alumno_2.apellido);
	printf("Nombre alumno 2: %s \r\n", alumno_2.nombre);

	return 0;
}

/*==================[end of file]============================================*/

