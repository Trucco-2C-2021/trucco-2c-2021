########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 1_proyecto
#NOMBRE_EJECUTABLE = 1_proyecto.exe

#PROYECTO_ACTIVO = 3_proyecto
#NOMBRE_EJECUTABLE = 3_proyecto.exe

#PROYECTO_ACTIVO = 7_proyecto
#NOMBRE_EJECUTABLE = 7_proyecto.exe

#PROYECTO_ACTIVO = 9_proyecto
#NOMBRE_EJECUTABLE = 9_proyecto.exe

#PROYECTO_ACTIVO = 12_proyecto
#NOMBRE_EJECUTABLE = 12_proyecto.exe

PROYECTO_ACTIVO = 14_proyecto
NOMBRE_EJECUTABLE = 14_proyecto.exe

#PROYECTO_ACTIVO = 16_proyecto
#NOMBRE_EJECUTABLE = 16_proyecto.exe

#PROYECTO_ACTIVO = 17_proyecto
#NOMBRE_EJECUTABLE = 17_proyecto.exe

#PROYECTO_ACTIVO = 18_proyecto
#NOMBRE_EJECUTABLE = 18_proyecto.exe

#PROYECTO_ACTIVO = integrador_proyecto1_a
#NOMBRE_EJECUTABLE = integrador_proyecto1_a.exe

#PROYECTO_ACTIVO = integrador_proyecto1_c
#NOMBRE_EJECUTABLE = integrador_proyecto1_c.exe

#PROYECTO_ACTIVO = integrador_proyecto1_d
#NOMBRE_EJECUTABLE = integrador_proyecto1_d.exe


#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe
