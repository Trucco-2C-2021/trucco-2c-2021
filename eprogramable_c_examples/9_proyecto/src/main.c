/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio 9
 * 
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
    const uint32_t constante=1;
    uint32_t copy;
    uint8_t A;
    copy=constante;
    if(copy==0){
    	A=0;
    	printf("El valor del bit 4 es %d", A);
    }
    else {
    	A=0xaa;
    	printf("El valor del bit 4 es %d",A);
    }
	return 0;
}

/*==================[end of file]============================================*/

