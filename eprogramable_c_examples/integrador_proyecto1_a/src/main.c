/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio Integrador - Inciso A
 * 
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"
#include "math.h"
/*==================[macros and definitions]=================================*/

#define LED1 1
#define LED2 2
#define LED3 3
#define ON 1
#define OFF 2
#define TOGGLE 3

typedef struct {
	uint8_t n_led;      //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON, OFF, TOGGLE
} my_leds;

void ControlLed(my_leds *var);

/*==================[internal functions declaration]=========================*/

void ControlLed(my_leds *var) {
	uint8_t i;
	uint8_t j;
	switch (var->mode) {

	case ON:
		switch (var->n_led) {
		case LED1:
			printf("Se prende el led %d", var->n_led);
			break;
		case LED2:
			printf("Se prende el led %d", var->n_led);
			break;
		case LED3:
			printf("Se prende el led %d", var->n_led);
			break;

		}
		break;

	case OFF:
		switch (var->n_led) {
		case LED1:
			printf("Se apaga el led %d", var->n_led);
			break;
		case LED2:
			printf("Se apaga el led %d", var->n_led);
			break;
		case LED3:
			printf("Se apaga el led %d", var->n_led);
			break;

		}
		break;
	case TOGGLE:

		for (i; i < var->n_ciclos; i++) {
			switch (var->n_led) {
			case LED1:
				printf("Se togglea led 1 %d", var->n_led);
				break;
			case LED2:
				printf("Se togglea el led %d", var->n_led);
				break;
			case LED3:
				printf("Se togglea el led %d", var->n_led);
				break;
			}
			for (j = 0; j < var->periodo; j++) {
			}
		}
		break;
	}

}

int main(void) {

	my_leds led_empleado = { 2, 2, 2, 3 };
	ControlLed(&led_empleado);

	return 0;
}

/*==================[end of file]============================================*/

