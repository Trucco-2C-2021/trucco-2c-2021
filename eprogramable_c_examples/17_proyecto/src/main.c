/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio 17
 * 
 *
 *
*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

#define TOTALNUMEROS 15
const uint8_t todos_los_numeros[]={234, 123, 111, 101, 32, 116, 211, 24, 214, 100, 124, 222, 1, 129, 9};
 /*Utilizo 16 ya que evaluando el peor de los casos seran 255x15=3825 y me entraran todos los elementos de la suma*/
uint16_t suma;
uint8_t i;
uint8_t valor_promedio;

/*==================[internal functions declaration]=========================*/

int main(void)
{
   for(i=0; i<TOTALNUMEROS; i++) {
	   suma+=todos_los_numeros[i];

   }
   printf("La suma es %d\r\n", suma);
   valor_promedio=(uint8_t)(suma/TOTALNUMEROS);
   printf("El promedio es %d\r\n", valor_promedio);



}

/*==================[end of file]============================================*/

