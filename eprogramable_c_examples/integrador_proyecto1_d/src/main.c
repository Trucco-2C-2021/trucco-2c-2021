/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio Integrador - Inciso d
 * 
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
typedef struct{
	uint8_t port;
	uint8_t pin;
	uint8_t dir;
} gpioConf_t;

/*==================[internal functions declaration]=========================*/
void BcdToLcd(uint8_t digito, gpioConf_t * configuracion){
	uint8_t i;
	for (i=0; i<10; i++) {
		digito=digito>>1;
		if(digito & 1){
			printf("Se pone en 1 el pin del puerto %d\n\n", configuracion[i].pin, configuracion[i].port);

		}
		else{
			printf("Se pone en 0 el pin del puerto %d\n\n", configuracion[i].pin, configuracion[i].port);

		}
	}

}
int main(void)
{
    uint8_t numeros[4];
    gpioConf_t conf[]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};
    uint8_t j;
    for (j=0; j<3; j++){
    	BcdToLcd(numeros[j], conf);
    }
	return 0;
}

/*==================[end of file]============================================*/

