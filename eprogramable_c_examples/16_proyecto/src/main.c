/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio 16
 * 
 *
 *
*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/

/*Inciso A*/
uint32_t var=0x01020304;
uint8_t a, b, c, d;

/*Inciso B*/

union datos{
	struct bytes{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}byte;
	uint32_t todos_los_bytes;
}union_var;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	/*Inciso A*/
	 a=var;
	 b = var>>8;
	 c=var>>16;
	 d= var>>24;

	 printf("La variable a contienen %d\r\n", a);
	 printf("La variable b contienen %d\r\n", b);
	 printf("La variable c contiene %d\r\n", c);
	 printf("La variable d contiene %d\r\n\n\n", d);

		/*Inciso B*/

	 union_var.todos_los_bytes=0x01020304;
	 printf("La union contiene %d\r\n\n", union_var.todos_los_bytes);
	 printf("El variable a extraida de la union es %d\r\n", union_var.byte.byte1);
	 printf("El variable b extraida de la union es %d\r\n", union_var.byte.byte2);
	 printf("El variable c extraida de la union es %d\r\n", union_var.byte.byte3);
	 printf("El variable d extraida de la union es %d\r\n", union_var.byte.byte4);





}

/*==================[end of file]============================================*/

