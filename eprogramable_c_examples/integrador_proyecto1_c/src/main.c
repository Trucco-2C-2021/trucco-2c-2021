/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio Integrador - Inciso C
 * 
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

int8_t BinaryToBcd(uint32_t data, uint8_t digits, uint8_t * bcd_number)
{
	while(digits > 0) {
		digits --;            //Corta el ciclo
		//printf("Digitos:%d\r\n", digits);
		bcd_number [digits]=data%10;
		printf("Resto:%d\r\n", bcd_number [digits]);
		data=data/10;
	}
}
/*==================[internal functions declaration]=========================*/

int main(void)
{
    uint8_t bcd_number[4];
    uint32_t dato=1234;
    uint8_t digits=4;
    printf("El dato es: %d\r\n", dato);
    BinaryToBcd(dato,digits,bcd_number);
	return 0;
}

/*==================[end of file]============================================*/

