/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Teresita Trucco
 * Proyecto 1: Ejercicio 12
 * 
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"

/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/

int main(void) {
	int16_t *punteroValor;
	int16_t valor = -1;
	punteroValor = &valor;
	printf("El valor de la variable es: %d\n", *punteroValor);
	int16_t mascara=0  | (1<<4);
	 mascara=~mascara;
	 *punteroValor&=mascara;
	printf("El valor de la variable es ahora: %d\n", *punteroValor);
	printf("La direccion de memoria es: %p", &punteroValor);

	return 0;
}

/*==================[end of file]============================================*/

